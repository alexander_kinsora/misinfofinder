import re

def add_encapsulator(filename):

    with open(filename,'rb') as readfile:

        with open(filename[:-4] + "_encap" + filename[-4:], 'wb') as writefile:

            i = 1
            for line in readfile.readlines():

                if i != 1:
                    modline = re.sub("(\t)(?!.*\t)", " ‰\t", line.decode('Latin1'))
                    fifth_field_start_idx = re.search('([\w,\"\'\ &-–\./\?!:;@=<>%+*^$#\[\]_]+\t|\t){4}', modline).span(0)[1]
                    fifth_field_end_idx = re.search('([\w,\"\'\ &-–\./\?!:;@=<>%+*^$#\[\]_]+\t|\t){5}', modline).span(0)[1] - 1
                    modline = modline[:fifth_field_start_idx] + "‰" + modline[fifth_field_start_idx:fifth_field_end_idx]\
                                  + "‰" + modline[fifth_field_end_idx:]
                    #we add 3, since we also inserted two encapsulators
                    fifth_tab_end_idx = fifth_field_end_idx + 3
                    line_to_add = modline[:fifth_tab_end_idx] + '‰' + modline[fifth_tab_end_idx:]
                else:
                    modline = line.decode('Latin1')
                    line_to_add = modline
                writefile.write(line_to_add.encode('UTF-8'))
                i += 1
                if i % 100000 == 0:
                    print(i)

def main():

    filename = '/home/alexander/Documents/devel/misinfofinder/data/new_comment_table_1.csv'
    add_encapsulator(filename)
    print('Done!')
    # Command line args are in sys.argv[1], sys.argv[2] ..
    # sys.argv[0] is the script name itself and can be ignored

# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':

    main()