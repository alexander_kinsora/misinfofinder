# coding=utf-8
import csv
import os
import string
import sys
import copy
from collections import defaultdict
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize

ERROR = 1
SUCCESS = 0
LIST_FILENAMES = ["equivocation.blacklist", "hearsay.blacklist", "initialism.whitelist"]
header_index_map = {"question_post_id": "", "comment_post_id": "", "comment_text": "", "date": "", "label": ""}
index = {}
data_onehot = {}
onehot_temp = {}
whiteblacklist_map = {}


def parse_delimited_file(filepath):
    if os.path.exists(filepath):
        with open(filepath, mode='r') as f:
            char_sep = csv.reader(f, delimiter='\t', quotechar='‰')
            valid_field_names = [key for key in header_index_map.keys()]
            for i, line in enumerate(char_sep):
                if i == 0:
                    for j, header in enumerate(line):
                        if header in header_index_map.keys():
                            header_index_map[header] = j
                            header_index_map[str(j)] = header
                else:
                    break
            if len(header_index_map) != len(valid_field_names) * 2:
                print(
                    'Invalid number of fields detected, or your file has no header. Make sure that the column headers '
                    + '"question_post_id", "comment_post_id", "comment_text", "date", and "label" are present.')
                return ERROR
            else:
                return [{header_index_map[str(j)]: field.strip() for j, field in enumerate(line)
                         if str(j) in header_index_map.keys()}
                        for i, line in enumerate(char_sep)
                        if i != 0]
    else:
        return ERROR


def load_file(args):
    if len(args) == 0:
        print("Please provide tab-separated file, delimited by '‰', to load.")
        return ERROR
    filepath = args[1]
    data = parse_delimited_file(filepath)
    if data == ERROR:
        print("File failed to load properly. Make sure that the path is valid, the file is tab-delimited, and the " \
              "quoting character is '‰'.")
        return ERROR
    return data


def separate_by_thread(data):
    threads = dict()
    for item in data:
        if item["question_post_id"] not in threads.keys():
            threads.update({item["question_post_id"]: []})
        threads[item["question_post_id"]].append(item)
    return threads


def determine_last_misinfo_distance(thread, comment_no, last_misinfo_distance):
    if comment_no != 0:
        # now let's see if there
        if thread[comment_no - 1]["label"] == str(1):
            return 1
        else:
            if thread[comment_no - 1]["last_misinfo_dist_score"] != -1.0:
                return last_misinfo_distance + 1
            else:
                return -1
    else:
        return -1


def update_index(comment, lemmatizer):
    tokenized_comment_text = word_tokenize(comment["comment_text"].translate({ord(c): None
                                                                              for c in string.punctuation}))
    lemmatized_text = [lemmatizer.lemmatize(word).strip().upper() for word in tokenized_comment_text]
    for pos, word in enumerate(lemmatized_text):
        if word not in index.keys():
            index[word] = [(comment["id"], pos)]
        else:
            index[word].append((comment["id"], pos))


# builds a dictionary which will serve as the base for a one-hot representation of the comments
def initialize_onehot_temp(data, lemmatizer):
    for id, comment in enumerate(data):
        comment["id"] = id
        tokenized_comment_text = word_tokenize(comment["comment_text"].translate({ord(c): None
                                                                                  for c in string.punctuation}))
        lemmatized_text = [lemmatizer.lemmatize(word).strip().upper() for word in tokenized_comment_text]
        for word in lemmatized_text:
            onehot_temp[word] = 0


def add_last_misinfo_dist_score(comment, last_misinfo_distance, thread_length):
    if last_misinfo_distance == -1:
        last_misinfo_dist_score = -1.0
    else:
        length_modifier = 1.0 / float(thread_length)
        last_misinfo_dist_score = 1 - (float(last_misinfo_distance - 1) * length_modifier)
    comment["last_misinfo_dist_score"] = last_misinfo_dist_score


def add_comment_length(comment):
    comment["comment_length"] = len(comment["comment_text"])


def determine_running_misinfo_count(comment, misinfo_count):
    return misinfo_count + 1 if comment["label"] == str(1) else misinfo_count


def add_running_misinfo_count(comment, misinfo_count):
    comment["running_misinfo_count"] = misinfo_count


def add_features(data):
    lemmatizer = WordNetLemmatizer()
    threads = separate_by_thread(data)
    # NOTE: Adds id field to data
    initialize_onehot_temp(data, lemmatizer)
    for thread_key in threads.keys():
        thread = threads[thread_key]
        last_misinfo_distance = 0
        misinfo_count = 0
        for comment_no, comment in enumerate(thread):
            update_index(comment, lemmatizer)
            last_misinfo_distance = determine_last_misinfo_distance(thread, comment_no, last_misinfo_distance)
            misinfo_count = \
                determine_running_misinfo_count(thread[comment_no - 1], misinfo_count) if comment_no != 0 else 0
            add_running_misinfo_count(comment, misinfo_count)
            add_last_misinfo_dist_score(comment, last_misinfo_distance, len(thread))
            add_comment_length(comment)
            add_nlp_features(comment, lemmatizer)
    return threads

def get_simple_capitalization_percent(comment_text):
    return float(sum(char.isupper() for char in comment_text)) / float(sum(1 for char in comment_text.replace(" ", "")
                                                                           .replace("\t", "").replace("\r", "")
                                                                           .replace("\n", ""))) if comment_text != "" \
        else 0.0


def add_nlp_features(comment, lemmatizer):
    # remove comment text and tokenize it
    tokenized_comment_text = word_tokenize(comment["comment_text"].translate({ord(c): None
                                                                              for c in string.punctuation}))
    lemmatized_comment_text = [lemmatizer.lemmatize(word) for word in tokenized_comment_text]
    # percent of capitalized characters after whitespace stripped
    comment["simple_capitalization_percent"] = get_simple_capitalization_percent(comment["comment_text"])
    comment["all_caps_word_count"] = add_all_caps_word_percent(lemmatized_comment_text)
    comment["equivocation_terms"] = get_blacklist_terms_in_text(comment["comment_text"].translate({ord(c): None
                                                                                                   for c in
                                                                                                   string.punctuation}).upper(),
                                                                whiteblacklist_map["equivocation.blacklist"])
    comment["hearsay_terms"] = get_blacklist_terms_in_text(comment["comment_text"].translate({ord(c): None
                                                                                              for c in
                                                                                              string.punctuation}).upper(),
                                                           whiteblacklist_map["hearsay.blacklist"])
    # features associated with indexing not yet complete
    # capitalize everything; we're about to search for terms
    # cap_lem_comment_text = [word.upper() for word in lemmatized_comment_text]
    # take this opportunity to add the one-hot vector for this comment
    # one_hot = copy.copy(onehot_temp)
    # for word in cap_lem_comment_text:
    #    one_hot[word] = 1
    # data_onehot[comment["id"]] = one_hot


def get_blacklist_terms_in_text(comment_text, blacklist):
    present = 0
    for term in blacklist:
        present += comment_text.count(term)
    return present

# get the number of words in all caps as a percentage of words in the comment
def add_all_caps_word_percent(lemmatized_comment_text):
     return float(sum(word.isupper() for word in lemmatized_comment_text
                      if word not in whiteblacklist_map["initialism.whitelist"])) / \
            float(sum(1 for word in lemmatized_comment_text)) if len(lemmatized_comment_text) > 0 else 0.0


# expects files of the following names in the directory in which the program is run:
# ./equivocation.blacklist, ./hearsay.blacklist, ./initialism.whitelist
# if they are not found, they will not be loaded.
# each line will be lemmatized, capitalized and treated as a search phrase for the relevant list; for example, if "i might" is
# entered on one line for equivocation.blacklist, the occurances of "I MIGHT" will be counted in the capitalized
# comment_text field of each comment. All characters are allowed, but each line will be trimmed.
def load_whiteblacklist_files():
    lemmatizer = WordNetLemmatizer()
    for filename in LIST_FILENAMES:
        filepath = "./" + filename
        if os.path.exists(filepath):
            with open(filepath, 'r') as f:
                # we use an extremely uncommon character for the delimiter and quoting char,
                # because we want everything to be captured on each line.
                linereader = csv.reader(f, delimiter='Œ', quotechar='‰')
                whiteblacklist = [" ".join([lemmatizer.lemmatize(word).strip().upper()
                                   for word in word_tokenize(line[0].translate({ord(c): None
                                                                                for c in string.punctuation}))])
                                  for line in linereader
                                  ]
                whiteblacklist_map[filename] = whiteblacklist

def write_csv(data_as_list):
    with open("data_output.tsv", 'w') as tsvfile:
        fieldnames = list(data_as_list[0].keys())
        writer = csv.DictWriter(tsvfile, fieldnames=fieldnames, delimiter='\t', quotechar='‰')
        writer.writeheader()
        for row in data_as_list:
            writer.writerow(row)

# Gather our code in a main() function
def main(args):
    data = load_file(args)
    load_whiteblacklist_files()
    if data != ERROR:
        data = add_features(data)
        data_as_list = []
        for question in data.keys():
            for comment in data[question]:
                data_as_list.append(comment)
        write_csv(data_as_list)
        return SUCCESS
    else:
        return ERROR


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main(sys.argv)
